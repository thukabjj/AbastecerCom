﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            btnCal.Clicked += BtnCal_Clicked;
        }

        private void BtnCal_Clicked(object sender, EventArgs e)
        {
            double gasolina = Convert.ToDouble(txtGas.Text);
            double etanol = Convert.ToDouble(txtEta.Text);
            double result = 0;

            if (gasolina > 0 && etanol > 0)
            {
                result = etanol / gasolina;
                if (result > 0.7)
                {
                    imgC.Source = Device.RuntimePlatform == Device.Android ? ImageSource.FromFile("gasolina.png") : ImageSource.FromFile("Images/gasolina.png");
                }
                else
                {

                    imgC.Source = Device.RuntimePlatform == Device.Android ? ImageSource.FromFile("etanol.png") : ImageSource.FromFile("Images/etanol.png");
                }
            }
            else {
                imgC.Source = Device.RuntimePlatform == Device.Android ? ImageSource.FromFile("neutro.png") : ImageSource.FromFile("Images/neutro.png");
            }
        }
    }
}
